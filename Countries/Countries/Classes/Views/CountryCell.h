	//
	//  CountryCell.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <UIKit/UIKit.h>
#import "Country.h"

@interface CountryCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *population;
@property (nonatomic, weak) IBOutlet UILabel *localisedName;
@property (nonatomic, weak) IBOutlet UILabel *region;
@property (nonatomic, weak) IBOutlet UIImageView *flagImgView;
-(void)configure:(Country *)country;
@end
