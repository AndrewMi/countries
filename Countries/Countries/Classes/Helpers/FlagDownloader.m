	//
	//  FlagDownloader.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "FlagDownloader.h"

@interface FlagDownloader ()

@property (nonatomic, strong) NSURLSessionDataTask *sessionTask;

@end

static NSString* const flagBaseUrl = @"http://www.geonames.org/flags/x/";

@implementation FlagDownloader

- (void)startDownload
{
	NSString * alphaCode = [self.country.alpha2Code  lowercaseString];
	NSString *urlstr = [NSString stringWithFormat:@"%@%@.gif",flagBaseUrl, alphaCode];
	NSURL	* url = [NSURL URLWithString:urlstr];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	
	_sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:request
												   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
													   
													   if (error){
														   return;
													   }
													   dispatch_async(dispatch_get_main_queue(),^{
														   
														   UIImage *image = [[UIImage alloc] initWithData:data];
														   
														   self.country.flagImg = image;
														   
														   if (self.completionHandler != nil)
															 {
															   self.completionHandler();
															 }
													   });
												   }];
	[self.sessionTask resume];
}

- (void)cancelDownload
{
	[self.sessionTask cancel];
	_sessionTask = nil;
}

@end
