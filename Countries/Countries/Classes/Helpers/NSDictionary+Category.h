//
//  NSDictionary+Category.h
//  Countries
//
//  Created by Andrei Mirzac on 04/07/2016.
//  Copyright © 2016 Andrei Mirzac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Category)
- (id)validatedValueForKey:(NSString *)key;
@end
