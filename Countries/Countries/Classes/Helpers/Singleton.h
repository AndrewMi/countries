	//
	//  APIService.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 02/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <Foundation/Foundation.h>
#import "Country.h"

@interface Singleton : NSObject

+ (instancetype)sharedInstance;

typedef void(^failure)(NSError * error);
typedef void(^success)(NSArray * response);

@property(nonatomic,strong)NSArray *countries;

-(void)fetchCountries:(success)success failure:(failure)failure;
-(void)sortCountriesByNameAlphabetically;
-(NSArray *)filterByRegion:(NSString *)region;
-(Country *)countryWith3Code:(NSString *)code;
@end
