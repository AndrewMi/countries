//
//  NSDictionary+Category.m
//  Countries
//
//  Created by Andrei Mirzac on 04/07/2016.
//  Copyright © 2016 Andrei Mirzac. All rights reserved.
//

#import "NSDictionary+Category.h"

@implementation NSDictionary (Category)

- (id)validatedValueForKey:(NSString *)key {
	
	id value = [self valueForKey:key];
	if (value == [NSNull null]) {
		value = nil;
	}
	return value;
}
@end