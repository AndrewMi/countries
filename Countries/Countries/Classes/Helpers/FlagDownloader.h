	//
	//  FlagDownloader.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <Foundation/Foundation.h>
#import "Country.h"

@interface FlagDownloader : NSObject

@property (nonatomic, strong) Country *country;
@property (nonatomic, copy) void (^completionHandler)(void);

- (void)startDownload;
- (void)cancelDownload;
@end
