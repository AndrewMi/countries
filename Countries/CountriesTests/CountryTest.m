	//
	//  CountryTest.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <XCTest/XCTest.h>
#import "Country.h"
#import "Singleton.h"


@interface CountryTest : XCTestCase

@end

@implementation CountryTest

- (void)testFilterCountriesByRegion {
	
	Singleton * singleton =  [Singleton sharedInstance];
	singleton.countries = [self allCountries];
	NSArray * array = [singleton filterByRegion:@"Europe"];
	XCTAssertEqual(array.count, 52);
	
	array = [singleton filterByRegion:@"Asia"];
	XCTAssertEqual(array.count, 50);
	
	array = [singleton filterByRegion:@"Africa"];
	XCTAssertEqual(array.count, 59);
	
	array = [singleton filterByRegion:@"Americas"];
	XCTAssertEqual(array.count, 56);
	
	
	array = [singleton filterByRegion:@"dsad"];
	XCTAssertEqual(array.count, 0);
}

- (void)testGetCountryByAbrevitation {
	
	Singleton * singleton =  [Singleton sharedInstance];
	singleton.countries = [self allCountries];
	
	Country * country = [singleton countryWith3Code:@"AFG"];
	XCTAssert([country.details.alpha3Code isEqualToString:@"AFG"]);
	
	country = [singleton countryWith3Code:@""];
	XCTAssertNil(country);
}

-(void)testSortCountriesAlphabetically {
	
	Singleton * singleton =  [Singleton sharedInstance];
	singleton.countries = [self allCountries];
	[singleton sortCountriesByNameAlphabetically];
	
	Country* country = singleton.countries[0];
	XCTAssert([country.alpha2Code isEqualToString:@"AD"]);
	
	country = [singleton.countries lastObject];
	XCTAssert([country.alpha2Code isEqualToString:@"ZW"]);
}

	//MARK:Helpers
-(NSArray *)allCountries{
	
	NSArray * countriesJSON = [self loadAllCountries];
	
	NSMutableArray * array = [NSMutableArray new];
	for (NSDictionary * dict in countriesJSON ) {
		Country * country =  [[Country alloc]initWithDictionary:dict];
		[array addObject:country];
	}
	return array;
}

-(NSDictionary *)loadCountryFromFile:(NSString* )fileName {
	NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:fileName ofType:@"json"];
	NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath
													   encoding:NSUTF8StringEncoding
														  error:NULL];
	NSData * data = [myJSON dataUsingEncoding:NSUTF8StringEncoding];
	
	return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

-(NSArray *)loadAllCountries {
	NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"allCountries" ofType:@"json"];
	NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath
													   encoding:NSUTF8StringEncoding
														  error:NULL];
	NSData * data = [myJSON dataUsingEncoding:NSUTF8StringEncoding];
	
	return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}
@end
