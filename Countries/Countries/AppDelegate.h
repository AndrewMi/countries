	//
	//  AppDelegate.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 02/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

