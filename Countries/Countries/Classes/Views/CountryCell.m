	//
	//  CountryCell.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "CountryCell.h"

@implementation CountryCell

-(void)configure:(Country *)country {
	self.population.text = [NSString stringWithFormat:@"Population:%@",country.population.stringValue];
	self.region.text = country.region;
	self.localisedName.text = country.alpha2Code;
	self.flagImgView.image = [UIImage imageNamed:@"placeholder"];
}
@end
