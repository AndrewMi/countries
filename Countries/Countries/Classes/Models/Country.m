	//
	//  Country.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 02/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "Country.h"

@implementation Country

-(instancetype)initWithDictionary:(NSDictionary *)dict {
	
	self = [super init];
	if (self) {
		_population = [dict objectForKey:@"population"];
		_alpha2Code = [dict objectForKey:@"alpha2Code"];
		_region = [dict objectForKey:@"region"];
		_details = [[CountryDetails alloc]initWithDictionary:dict];
	}
	return self;
}
@end
