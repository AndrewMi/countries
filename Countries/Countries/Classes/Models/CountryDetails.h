	//
	//  CountryDetails.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <Foundation/Foundation.h>

@interface CountryDetails : NSObject

@property (readonly,strong,nonatomic) NSString * name;
@property (readonly,strong,nonatomic) NSString * capital;
@property (readonly,strong,nonatomic) NSArray * altSpellings;
@property (readonly,strong,nonatomic) NSDictionary * translations;
@property (readonly,strong,nonatomic) NSString * relevance;
@property (readonly,strong,nonatomic) NSString * subregion;
@property (readonly,strong,nonatomic) NSArray * latlng;
@property (readonly,strong,nonatomic) NSString * demonym;
@property (readonly,strong,nonatomic) NSNumber * area;
@property (readonly,strong,nonatomic) NSNumber * gini;
@property (readonly,strong,nonatomic) NSArray * timezones;
@property (readonly,strong,nonatomic) NSArray * borders;
@property (readonly,strong,nonatomic) NSString * nativeName;
@property (readonly,strong,nonatomic) NSArray * callingCodes;
@property (readonly,strong,nonatomic) NSString * alpha3Code;
@property (readonly,strong,nonatomic) NSArray * currencies;
@property (readonly,strong,nonatomic) NSArray * languages;
@property (readonly,strong,nonatomic) NSArray * topLevelDomain;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end
