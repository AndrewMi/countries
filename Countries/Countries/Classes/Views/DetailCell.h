//
//  DetailCell.h
//  Countries
//
//  Created by Andrei Mirzac on 04/07/2016.
//  Copyright © 2016 Andrei Mirzac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *value;

-(void)configure:(NSString *)title value:(NSString *)value;
@end
