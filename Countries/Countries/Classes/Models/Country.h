	//
	//  Country.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 02/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CountryDetails.h"

@interface Country : NSObject

@property (readonly,strong,nonatomic) NSString * alpha2Code;
@property (readonly,strong,nonatomic) NSString * region;
@property (readonly,strong,nonatomic) NSNumber * population;
@property (nonatomic, strong) UIImage* flagImg;
@property (readonly, strong, nonatomic) CountryDetails * details;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
@end



