	//
	//  DetailsViewController.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 04/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "DetailsViewController.h"
#import "CountriesViewController.h"
#import "Country.h"
#import "DetailCell.h"
#import "Singleton.h"

static NSString * DetailViewControllerId = @"detailViewControllerId";
static NSString * CountriesViewControllerId = @"countriesViewControllerId";
static NSString * detailIdentifier = @"detailIdentifier";
static NSInteger NrSections = 4;
static NSInteger NrRowsMainSection = 6;
static NSInteger NrRowsOtherSection = 4;

typedef NS_ENUM(NSInteger, SectionType) {
	Main,
	Borders,
	Others
};

@implementation DetailsViewController

	//MARK:View Life Cycle
-(void)viewDidLoad {
	[super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}

	//MARK:UITableViewDelegates
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
	if (cell.accessoryType == UITableViewCellAccessoryDisclosureIndicator) {
		
		if (indexPath.section == 0){ //Region selected
			[self showCountriesFromRegion];
		}else {//Border selected
			[self showDetailsFor:self.country.details.borders[indexPath.row]];
		}
	}
}
- (void)showCountriesFromRegion {
	
	NSString * region = self.country.region;
	
	CountriesViewController* countriesVC = [self.storyboard instantiateViewControllerWithIdentifier:CountriesViewControllerId];
	countriesVC.countries = [[Singleton sharedInstance]filterByRegion:region];
	countriesVC.title = region;
	[self.navigationController pushViewController:countriesVC animated:YES];
}

- (void)showDetailsFor:(NSString *)borderCountry {
	
	DetailsViewController * detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:DetailViewControllerId];
	detailsVC.country = [[Singleton sharedInstance]countryWith3Code:borderCountry];
	detailsVC.title = detailsVC.country.details.name;
	
	[self.navigationController pushViewController:detailsVC animated:YES];
}

	//MARK:UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	DetailCell * cell = (DetailCell *)[tableView dequeueReusableCellWithIdentifier:detailIdentifier];
	SectionType sectionType = indexPath.section;
	
	switch (sectionType) {
		case Main:
			switch (indexPath.row) {
				case 0: [cell configure:@"Name:" value:self.country.details.name];break;
				case 1: [cell configure:@"Capital:" value:self.country.details.capital];break;
				case 2: [cell configure:@"Population:" value:self.country.population.stringValue];break;
				case 3: [cell configure:@"Currencies:" value:[self.country.details.currencies componentsJoinedByString:@","]];break;
				case 4: [cell configure:@"Languages:" value:[self.country.details.languages componentsJoinedByString:@","]];break;
				case 5: {
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					[cell configure:@"Region:" value:self.country.region];break;
				}break;
			}break;
		case Borders: {
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			[cell configure:@"" value:self.country.details.borders[indexPath.row]];break;
		}break;
			
		case Others:
			switch (indexPath.row) {
				case 0: [cell configure:@"Timezone:" value:[self.country.details.timezones componentsJoinedByString:@","]];break;
				case 1: [cell configure:@"Area:" value:self.country.details.area.stringValue];break;
				case 2: [cell configure:@"LatLong:" value:[self.country.details.latlng componentsJoinedByString:@","]];break;
				case 3: [cell configure:@"NativeName:" value:self.country.details.nativeName];break;
				case 4: [cell configure:@"Demonym:" value:self.country.details.demonym]; break;
				case 5: [cell configure:@"CallingCodes:" value:[self.country.details.callingCodes componentsJoinedByString:@","]];break;
				case 6: [cell configure:@"TopLevelDomain:" value:[self.country.details.topLevelDomain componentsJoinedByString:@","]];break;
			}break;
			break;
	}
	return  cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return NrSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	SectionType sectionType = section;
	
	switch (sectionType) {
		case Main:return  NrRowsMainSection;
		case Borders:return  self.country.details.borders.count;
		case Others:return  NrRowsOtherSection;
	}
	return  0;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	SectionType sectionType = section;
	switch (sectionType) {
		case Main:return  @"Main";
		case Borders:return  @"Borders";
		case Others:return  @"Other";
	}
	return @"";
}


@end
