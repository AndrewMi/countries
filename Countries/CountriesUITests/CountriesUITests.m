//
//  CountriesUITests.m
//  CountriesUITests
//
//  Created by Andrei Mirzac on 02/07/2016.
//  Copyright © 2016 Andrei Mirzac. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface CountriesUITests : XCTestCase

@end

@implementation CountriesUITests

-(void)testTapCountryThenTapOnRegion{
	
	XCUIApplication *app = [[XCUIApplication alloc] init];
	XCUIElementQuery *tablesQuery = app.tables;
	[[tablesQuery.cells containingType:XCUIElementTypeStaticText identifier:@"AD"].staticTexts[@"Europe"] tap];
	[tablesQuery.staticTexts[@"Europe"] tap];
	[app.navigationBars[@"Europe"].buttons[@"Andorra"] tap];
	[app.navigationBars[@"Andorra"].buttons[@"Countries"] tap];
	
}

@end
