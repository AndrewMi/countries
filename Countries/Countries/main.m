//
//  main.m
//  Countries
//
//  Created by Andrei Mirzac on 02/07/2016.
//  Copyright © 2016 Andrei Mirzac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
