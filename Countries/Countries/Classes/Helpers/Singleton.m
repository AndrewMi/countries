	//
	//  APIService.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 02/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "Singleton.h"


static NSString* const countriesFullUrl = @"https://restcountries.eu/rest/v1/all";


@interface Singleton()
@end

@implementation Singleton

+ (instancetype)sharedInstance {
	static Singleton *sharedObj = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedObj = [[self alloc] init];
	});
	return sharedObj;
}

-(void)fetchCountries:(success)success failure:(failure)failure {
	
	NSURLSession * session = [NSURLSession sharedSession];
	NSURL * url = [NSURL URLWithString: countriesFullUrl];
	
	NSURLSessionDataTask * dataTask =
	[session dataTaskWithURL:url
				 completionHandler:^(NSData *data,
									 NSURLResponse *response,
									 NSError *error) {
					 if(error) {
						 failure(error);
					 }
					 NSError * parseError = nil;
					 NSArray *array =
					 [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
					 if(parseError == nil && array) {
						 [self initCountriesFrom:array];
						 [self sortCountriesByNameAlphabetically];
						 success(self.countries);
					 }else {
						 failure(parseError);
					 }
				 }];
	
	[dataTask resume];
}
-(void)initCountriesFrom:(NSArray *)array {
	NSMutableArray * mutableArray = [[NSMutableArray alloc]init];
	for (NSDictionary * dict in array) {
		Country * country = [[Country alloc]initWithDictionary:dict];
		[mutableArray addObject:country];
	}
	self.countries = [mutableArray copy];
}

-(void)sortCountriesByNameAlphabetically {
	NSSortDescriptor * sortAlphabet =
	[[NSSortDescriptor alloc]initWithKey:@"alpha2Code"
							   ascending:true
								selector:@selector(localizedStandardCompare:)];
	self.countries = [self.countries sortedArrayUsingDescriptors:@[sortAlphabet]];
}
-(NSArray *)filterByRegion:(NSString *)region {
	NSPredicate * predicate = [NSPredicate predicateWithFormat:@"region == %@",region];
	return  [self.countries filteredArrayUsingPredicate:predicate];
}
-(Country *)countryWith3Code:(NSString *)code {
	NSPredicate * predicate = [NSPredicate predicateWithFormat:@"details.alpha3Code contains %@",code];
	return  [self.countries filteredArrayUsingPredicate:predicate].firstObject;
}

@end
