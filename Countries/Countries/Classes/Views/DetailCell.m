//
//  DetailCell.m
//  Countries
//
//  Created by Andrei Mirzac on 04/07/2016.
//  Copyright © 2016 Andrei Mirzac. All rights reserved.
//

#import "DetailCell.h"

@implementation DetailCell

-(void)configure:(NSString *)title value:(NSString *)value {
	self.title.text = title;
	self.value.text = value;
}
@end
