	//
	//  CountryDetails.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 03/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "CountryDetails.h"
#import "NSDictionary+Category.h"
@implementation CountryDetails

-(instancetype)initWithDictionary:(NSDictionary *)dict {
	
	self = [super init];
	if (self) {
		_name = [dict validatedValueForKey:@"name"];
		_capital = [dict validatedValueForKey:@"capital"];
		_altSpellings = [dict validatedValueForKey:@"altSpellings"];
		_translations = [dict validatedValueForKey:@"translations"];
		_relevance = [dict validatedValueForKey:@"relevance"];
		_subregion = [dict validatedValueForKey:@"subregion"];
		_latlng = [dict validatedValueForKey:@"latlng"];
		_demonym = [dict validatedValueForKey:@"demonym"];
		_area = [dict validatedValueForKey:@"area"];
		_gini = [dict validatedValueForKey:@"gini"];
		_timezones = [dict validatedValueForKey:@"timezones"];
		_borders = [dict validatedValueForKey:@"borders"];
		_nativeName =[dict validatedValueForKey:@"nativeName"];
		_callingCodes = [dict validatedValueForKey:@"callingCodes"];
		_alpha3Code = [dict validatedValueForKey:@"alpha3Code"];
		_currencies = [dict validatedValueForKey:@"currencies"];
		_languages = [dict validatedValueForKey:@"languages"];
		_topLevelDomain = [dict validatedValueForKey:@"topLevelDomain"];
	}
	return self;
}
@end
