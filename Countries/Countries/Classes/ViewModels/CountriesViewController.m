	//
	//  CountriesViewController.m
	//  Countries
	//
	//  Created by Andrei Mirzac on 02/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import "CountriesViewController.h"
#import "DetailsViewController.h"
#import "CountryCell.h"
#import "Singleton.h"
#import "FlagDownloader.h"

static NSString * cellIndentifier = @"countryIdentifier";
static NSString * segueID = @"showDetails";

@interface CountriesViewController()
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;
@property (nonatomic,assign)NSIndexPath * selectedIndexPath;
@end

@implementation CountriesViewController

	//View Life Cycle
-(void)viewDidLoad {
	[super viewDidLoad];
	_imageDownloadsInProgress = [[NSMutableDictionary alloc]init];
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}

	//MARK:UITableViewDelegates
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	self.selectedIndexPath = indexPath;
	[self performSegueWithIdentifier:segueID sender:self];
}

	//MARK:UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	CountryCell * cell = (CountryCell *)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
	Country * country = self.countries[indexPath.row];
	[self startFlagDownload:country forIndexPath:indexPath];
	[cell configure:country];
	return  cell;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	Country* country = self.countries[indexPath.row];
	country.flagImg = nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return  self.countries.count;
}

	//MARK:Load images
- (void)startFlagDownload:(Country *)country forIndexPath:(NSIndexPath *)indexPath
{
	FlagDownloader *flagDownloader = (self.imageDownloadsInProgress)[indexPath];
	if (flagDownloader == nil)
	  {
		flagDownloader = [[FlagDownloader alloc] init];
		flagDownloader.country = country;
		[flagDownloader setCompletionHandler:^{
			
			CountryCell *cell = (CountryCell*)[self.tableView cellForRowAtIndexPath:indexPath];
			
				// Display the newly loaded image
			cell.flagImgView.image = country.flagImg;
			
				// Remove the FlagDownloader from progress list.
			[self.imageDownloadsInProgress removeObjectForKey:indexPath];
		}];
		(self.imageDownloadsInProgress)[indexPath] = flagDownloader;
		[flagDownloader startDownload];
	  }
}
	//MARK:Segue Prepare
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:segueID])
	  {
		DetailsViewController *detailViewController = [segue destinationViewController];
		Country  * country = self.countries[_selectedIndexPath.row];
		detailViewController.title = country.details.name;
		detailViewController.country = country;
	  }
}
@end
