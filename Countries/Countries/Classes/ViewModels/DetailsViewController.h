	//
	//  DetailsViewController.h
	//  Countries
	//
	//  Created by Andrei Mirzac on 04/07/2016.
	//  Copyright © 2016 Andrei Mirzac. All rights reserved.
	//

#import <UIKit/UIKit.h>
#import "Country.h"

@interface DetailsViewController : UITableViewController
@property(nonatomic,strong) Country * country;

@end
